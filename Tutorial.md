# Criando calendários compartilhaveis com Google Calendar

É possivel organizar nossos compromissos digitalmenter com a ajuda do Google calendar:

O google calendart contém algumas features interessantes:

- Calendários compartilhados 
- Multiplos calendários (para separarmos trabalho, escola e vida pessoal)
- lembretes
- confirmação de presença.

para usar o google calendar é preciso ter uma conta da google (se você usa celular Android provavelmente você ja tem uma) e ir no link https://calendar.google.com, ou utilizar o App (https://play.google.com/store/apps/details?id=com.google.android.calendar&hl=en)

A versão web é mais complexa e nos permite um gerenciamento melhor dos nossos contatos por isso vamos usar ela nesse tutorial.

Nesse tutorial vamos:

- Criar eventos Unicos
- Criar eventos recorrentes
- Compartilhar um calendário
- Adicionar um calendário público
- Criar dois calendários paralelos (um pessoal e um de faculdade)


## Começando:

acesse o [google calendar](https://calendar.google.com/)

Quando criamos uma conta google são altomaticamente criados alguns calendários:

- um calendário pessoal
- um lembrete de Aniversários (Baseado nos seus amigos do Google+ _se é que alguem usa google+_)
- Um calendário de lembretes
- Um calendário de tarefas

![](vazio.png)

## Visualizações

É possível visualizar diferentes períodos no google calendar

![](períodos.png)

escolha a visualização de semana para facilitar o resto do tutorial.

## Criando um evento Único

para criar um evento basta clicar no botão create ![](createButton.png)

ou clicar e arrastar o mouse sobre o período desejado do evento

um popup vai abrir confirmando o nome do nvento e o período

![](popupEvento.png)

Pronto esse evento está registrado no seu calendário

## Criando um Evento recorrente

para criar um evento recorrente começamos da mesma forma que um evento único, inserímos o nome e a data, mas clicamos em "Mais opções"

na aba de mais opções podemos adicionar convidados, horários para lembrete do evento entre outras coisas.

para fazer com o evento seja recorrente clique em "Não Repete" e escolha o tipo de recorrência:

![](eventoRecorrente.png)

o evento vai aparecer de forma recorrente, o Google Calendar vai confirmar, para todas alterações no evento, se a alteração vai ser "só dessa vez", "desse evento em diante" ou "para todas as vezes"

## Adicionando um calendário público

acima da lista de calendários temos um campo de adição de calendários, clique para ver o menu

![](addCalendars.png)

clique em "explorar calendários de interesse"

podemos escolher alguns dos calendários disponíveis

![](calendariosBasquete.png)

os eventos então vão aparecer na lista de ventos 

![](muitosEventos.png)

## Criando Calendários Paralelos

voltando para a aba de adição de calendários
vamos clicar dessa vez na opção "Criar novo Calendário"

![](createCalendar.png)


Inicialmente, quando criamos um calendário novo, nós somos os donos do calendário e outras pessoas não tem acesso aos eventos desse calendário

uma forma de adicionar eventos nesse calendário é deixar apenas ele visível e adicionar eventos normalmente.

para que outras pessoas possam ter acesso á esse calendário é preciso mudar as configurações do calendário:

![](optionsForCalendar.png)

aqui decidimos quem pode ver esse calendário e adicionar eventos

uma forma de compartilhar o calendário é criar um link compartilhável assim, qualquer pessoa que usar o link poderá adicionar o calendário na sua lista

![](shareCalendar.png)

podemos também adicionar pessoas específicas a partir do endereço de email, é possível também definir se essa pessoa pode, apenas saber se o tempo está livre ou ocupado, enxergar os detalhes dos eventos, adicionar e modificar eventos ou adicionar e modificar eventos e decidir quem tem acesso ao calendário.

